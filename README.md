# OpenML dataset: diamonds

https://www.openml.org/d/44979

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Description**

This classic dataset originally contained the prices and other attributes of almost 54,000 diamonds.
However, 14184 of those seem to be the same diamonds, measure from a different angle.
This can be found out but checking for duplicated value when disregarding the variables
x, y, z , depth and table, which are dependent on the angle.

**Attribute Information**

1. *price* - Content price price in US dollars (\$326--\$18,823), target feature
2. *carat* - weight of the diamond (0.2--5.01)
3. *cut* - quality of the cut (Fair, Good, Very Good, Premium, Ideal)
4. *color* - diamond colour, from J (worst) to D (best)
5. *clarity* - a measurement of how clear the diamond is (I1 (worst), SI2, SI1, VS2, VS1, VVS2, VVS1, IF (best))
6. *x* - length in mm (0--10.74)
7. *y* - width in mm (0--58.9)
8. *z* - depth in mm (0--31.8)
9. *depth* - total depth percentage = z / mean(x, y) = 2 * z / (x + y) (43--79)
10. *table* - width of top of diamond relative to widest point (43--95)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44979) of an [OpenML dataset](https://www.openml.org/d/44979). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44979/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44979/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44979/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

